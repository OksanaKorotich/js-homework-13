`use strict`;

// Робити приховування картинки та показ нової картинки поступовим (анімація fadeOut/fadeIn) протягом 0.5 секунди.

let pictures = Array.from(document.querySelectorAll('.image-to-show'));
console.log(pictures);

buttons = document.querySelector('.btn-wrapper');
let stopShow = document.querySelector('.btn-stop');
let showMustGoOn = document.querySelector('.btn-continue');
let timerID;

function showPictures(){

    buttons.classList.remove('hidden');

    timerID = setInterval(() =>{
        for(let i = 0; i < pictures.length; i++){
            if(pictures[i].classList.contains('active')){
                if(i < pictures.length){
                    pictures[i].classList.remove('active');
                    i++;
                    if(i === pictures.length){
                        pictures[i-1].classList.remove('active');
                        i = 0;
                    };
                    pictures[i].classList.add('active');
                };
            };
        };
    }, 3000)
};

showPictures();

stopShow.addEventListener('click', () => {clearInterval(timerID)});

showMustGoOn.addEventListener('click', () => {showPictures()});



















































